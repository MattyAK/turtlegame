import sys
import itertools
import numpy as np



NorthColors = ['r','r','r','o','o','g','r','r','s']
NorthSides =  ['h','t','t','t','h','t','h','h','t']
EastColors =  ['o','s','o','s','g','o','s','o','o']
EastSides =   ['h','t','t','t','t','t','h','h','t']
SouthColors = ['g','g','g','g','s','g','o','r','r']
SouthSides =  ['t','h','h','h','t','h','t','t','h']
WestColors =  ['s','o','s','r','r','s','g','s','g']
WestSides =   ['t','h','h','h','h','h','t','t','h']

class TurtlePart:
	def __init__(self):
		self.Color = "uninitialized"
		self.Side = "uninitialized"


class Card:
	def __init__(self, n):
		self.Num = n;
		self.North = TurtlePart()
		self.East = TurtlePart()
		self.South = TurtlePart()
		self.West = TurtlePart()
		self.finalConfig = 0
	def turnCW(self,n):
		for i in range(0,n):
			oldNorth = self.North
			oldEast = self.East
			oldSouth = self.South
			oldWest = self.West
			self.North = oldWest
			self.East = oldNorth
			self.South = oldEast
			self.West = oldSouth
			(self.finalConfig) += 1
			self.finalConfig %= 4
		return self



def createCards():
	nCards = 9
	AllCards = []
	for i in range (0,nCards):
		curCard = Card(i);
		curCard.North.Color = NorthColors[i]
		curCard.North.Side = NorthSides[i]
		curCard.East.Color = EastColors[i]
		curCard.East.Side = EastSides[i]
		curCard.South.Color = SouthColors[i]
		curCard.South.Side = SouthSides[i]
		curCard.West.Color = WestColors[i]
		curCard.West.Side = WestSides[i]
		curCard.finalConfig = 0
		AllCards.append(curCard)
	return AllCards



def validSoFar(Puzzle,nPlaced):
	newCard = Puzzle[nPlaced -1]
	newRow = int((nPlaced-1)/3)
	newCol = (nPlaced-1)%3
	if(newCol != 0):
		cardToWest = Puzzle[nPlaced -2]
		if(cardToWest.East.Color != newCard.West.Color):
			return False
		if(cardToWest.East.Side == newCard.West.Side):
			return False
	if(newRow != 0):
		cardToNorth = Puzzle[nPlaced-4]
		if(cardToNorth.South.Color != newCard.North.Color):
			return False
		if(cardToNorth.South.Side == newCard.North.Side):
			return False
	return True



def RecTryToSolve(nPlaced, curOrder, AllCards, Puzzle):
	if(nPlaced == 9):
		return [True, Puzzle]
	else:
		index = curOrder[nPlaced]
		curCard = AllCards[index]
		nPlaced += 1
		for i in range (0,4):
			turned = curCard.turnCW(i)
			Puzzle.append(turned)
			if(validSoFar(Puzzle, nPlaced)):
				if(RecTryToSolve(nPlaced, curOrder, AllCards, Puzzle)[0]):
					return [True, Puzzle]
			Puzzle.pop(nPlaced-1)
		nPlaced -= 1
		return [False, Puzzle]


def TryToSolve(curOrder, AllCards):
	nPlaced = 0
	Puzzle = []
	return (RecTryToSolve(nPlaced, curOrder, AllCards, Puzzle))

def rotate(li, ntimes):
	for i in range(0,ntimes):
		newList = list()
		newList.append(li[6])
		newList.append(li[3])
		newList.append(li[0])
		newList.append(li[7])
		newList.append(li[4])
		newList.append(li[1])
		newList.append(li[8])
		newList.append(li[5])
		newList.append(li[2])
		
		li = newList
	return li

def pSolution(Puzzle, nsuccess):
	i = 1
	print("\nSolution %d:" % nsuccess)
	for Card in Puzzle:
		print("\tSpot: %d    Card: %d    Orientation: %d" % (i, Card.Num, Card.finalConfig))
		i += 1
	
	print("\n")

 
if __name__=='__main__':
	n =  [0,1,2,3,4,5,6,7,8]
	AllCards = createCards()
	#print('length = ',len(AllCards))
	nsuccess = 0
	nredundant = 0
	successfulOrders = list();
	flag = 1;

	for curOrder in itertools.permutations(n):
		curOrder = list(curOrder)

		if((rotate(curOrder,1) not in successfulOrders) and (rotate(curOrder,2) not in successfulOrders) and (rotate(curOrder,3) not in successfulOrders)):
			response = TryToSolve(curOrder,AllCards)
			if(response[0]):
					nsuccess += 1
					pSolution(response[1], nsuccess)
					successfulOrders.append(curOrder)
	print 'number of successes = ', nsuccess
	
	